const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;
let password = '';
app.get('/encode/:text', (req, res) => {
  password = req.params.text;

  res.send(Vigenere.Cipher('password').crypt(password));

});
app.get('/decode/:text', (req, res) => {
  password = req.params.text;

  res.send(Vigenere.Decipher('password').crypt(password));

});
app.get('/', (req, res)=>{
  res.send('Write a "password" in address window')
});

app.listen(port, () => {

  console.log('We are live on http://localhost:' + port);

});
