const express = require('express');

const app = express();

const port = 8000;

app.get('/Hello', (req, res) => {

  res.send('<h1>Hello</h1>');

});
app.get('/', (req, res)=>{
  res.send('Write a "/Hello" in address window')
});
app.listen(port, () => {

  console.log('We are live on http://localhost:' + port);

});